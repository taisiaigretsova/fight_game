import { controls } from '../../constants/controls';
import { showWinnerModal } from './modal/winner';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    let firstFighterHealth = firstFighter.health;
    let secondFighterHealth = secondFighter.health;
    
    // firstFighter, secondFighter
    // resolve the promise with the winner when fight is over

    document.addEventListener('keypress', (e) => {
      const keyName = e.code;
      const firstPlayerAttack = controls.PlayerOneAttack;
      if (keyName === firstPlayerAttack) {
        secondFighterHealth -= getDamage(firstFighter, secondFighter);
        
        document.getElementById('right-fighter-indicator').style = `width: ${secondFighterHealth / secondFighter.health * 100}%`
        console.log(secondFighterHealth);
      }
      if (secondFighterHealth <= 0) resolve(firstFighter);
    })
    
    // p2.health - demage
    // p2.health < 0 -> resolve(p1)
    
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker)/* - getBlockPower(defender)*/;
  return damage;
}

export function getHitPower(fighter) {
  // return hit power
  return fighter.attack;
}

export function getBlockPower(fighter) {
  // return block power
  return fighter.defense;
}
